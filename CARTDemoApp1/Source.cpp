#include <Windows.h>

#include "../CARTLib/ClientAppImageType.h"

#define CART_APP_IMAGE_NAME "CARTDemoApp1.exe"
#define CART_APP_IMAGE_TYPE ClientAppImageType::EXE_BASE
#define CART_APP_FULL_NAME "MGP Client Reporting Tool Test App"
#define CART_APP_CODE_NAME "CARTDemoApp1"

#include "../CARTLib/CARTConfig.h"
#include "../CARTLib/CARTInit.h"

#include <iostream>

int main(int argc, char** argv) {
	CARTConfig* cartCfg = new CARTConfig();
	cartCfg->enableSilentReporting = false;
	cartCfg->enableCrashHandler = true;
	cartCfg->includeLogs = false;
	cartCfg->reportingServerHost = "localhost";
	cartCfg->reportingServerPort = 8081;

	InitCARTLib(cartCfg);

	cout << "Testing CART zero division detection...";

	InvokeCARTExceptionHandler(ExceptionType::MANUAL_DIV_BY_ZERO, "Test exception.");

	return 0;
}