#pragma once

#include "CARTLibExportDef.h"

#include "CARTAppInfo.h"

struct CARTConfig {
public:
	bool enableSilentReporting;
	char* reportingServerHost;
	unsigned short reportingServerPort;
	bool includeLogs;
	CARTAppInfo* appInfo;
	bool enableCrashHandler;
};