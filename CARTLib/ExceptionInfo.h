#pragma once

struct ExceptionInfo {
public:
	ExceptionInfo(char* _sourceFile, char* _sourceFunction, char* _sourceClass, unsigned int _sourceLine, char* _sourceThread, char* _description) {
		this->sourceFile = _sourceFile;
		this->sourceFunction = _sourceFunction;
		this->sourceClass = _sourceClass;
		this->sourceLine = _sourceLine;
		this->sourceThread = _sourceThread;
		this->description = _description;
	}

public:
	char* sourceFile;
	char* sourceFunction;
	char* sourceClass;
	unsigned int sourceLine;
	char* sourceThread;
	char* description;
};