#include "CARTWatchdog.h"

#define ToString(obj) #obj

CARTWatchdog::CARTWatchdog(CARTConfig* _cartConfig)
{
	this->cartConfig = new CARTConfig();
	CopyMemory(this->cartConfig, _cartConfig, sizeof(CARTConfig));

	InitEventReportManager(_cartConfig);

	if (_cartConfig->enableCrashHandler) InstallExceptionHandlers();
}


CARTWatchdog::~CARTWatchdog()
{
}

void CARTWatchdog::AddLogFile(char * _logFilePath)
{
}

void CARTWatchdog::HandleManualException(ExceptionType _eType, ExceptionInfo* _eInfo)
{
	HandleExceptionInternal(true, _eType, _eInfo);
}

LONG __stdcall CARTWatchdog::HandleSEH(PEXCEPTION_POINTERS pExceptionPtrs)
{
	string excpDescription = "Unhandled exception in module ";
	excpDescription.append(this->cartConfig->appInfo->imageName);
	excpDescription.append(": ");

	switch (pExceptionPtrs->ExceptionRecord->ExceptionCode) {
		case EXCEPTION_ACCESS_VIOLATION: {
			excpDescription.append(ToString(EXCEPTION_ACCESS_VIOLATION));
			break;
		}
		case EXCEPTION_ARRAY_BOUNDS_EXCEEDED: {
			excpDescription.append(ToString(EXCEPTION_ARRAY_BOUNDS_EXCEEDED));
			break;
		}
		case EXCEPTION_BREAKPOINT: {
			excpDescription.append(ToString(EXCEPTION_BREAKPOINT));
			break;
		}
		case EXCEPTION_DATATYPE_MISALIGNMENT: {
			excpDescription.append(ToString(EXCEPTION_DATATYPE_MISALIGNMENT));
			break;
		}
		case EXCEPTION_FLT_DENORMAL_OPERAND: {
			excpDescription.append(ToString(EXCEPTION_FLT_DENORMAL_OPERAND));
			break;
		}
		case EXCEPTION_FLT_DIVIDE_BY_ZERO: {
			excpDescription.append(ToString(EXCEPTION_FLT_DIVIDE_BY_ZERO));
			break;
		}
		case EXCEPTION_FLT_OVERFLOW: {
			excpDescription.append(ToString(EXCEPTION_FLT_OVERFLOW));
			break;
		}
		case EXCEPTION_FLT_STACK_CHECK: {
			excpDescription.append(ToString(EXCEPTION_FLT_STACK_CHECK));
			break;
		}
		case EXCEPTION_FLT_UNDERFLOW: {
			excpDescription.append(ToString(EXCEPTION_FLT_UNDERFLOW));
			break;
		}
		case EXCEPTION_ILLEGAL_INSTRUCTION: {
			excpDescription.append(ToString(EXCEPTION_ILLEGAL_INSTRUCTION));
			break;
		}
		case EXCEPTION_IN_PAGE_ERROR: {
			excpDescription.append(ToString(EXCEPTION_IN_PAGE_ERROR));
			break;
		}
		case EXCEPTION_INT_DIVIDE_BY_ZERO: {
			excpDescription.append(ToString(EXCEPTION_INT_DIVIDE_BY_ZERO));
			break;
		}
		case EXCEPTION_INT_OVERFLOW: {
			excpDescription.append(ToString(EXCEPTION_INT_OVERFLOW));
			break;
		}
		case EXCEPTION_NONCONTINUABLE_EXCEPTION: {
			excpDescription.append(ToString(EXCEPTION_NONCONTINUABLE_EXCEPTION));
			break;
		}
		case EXCEPTION_PRIV_INSTRUCTION: {
			excpDescription.append(ToString(EXCEPTION_PRIV_INSTRUCTION));
			break;
		}
		case EXCEPTION_SINGLE_STEP: {
			excpDescription.append(ToString(EXCEPTION_SINGLE_STEP));
			break;
		}
		case EXCEPTION_STACK_OVERFLOW: {
			excpDescription.append(ToString(EXCEPTION_STACK_OVERFLOW));
			break;
		}
		default: {
			excpDescription.append("Unknown exception");
			break;
		}
	}

	excpDescription.append(" (Address: ");
	excpDescription.append(ToString(&pExceptionPtrs->ExceptionRecord->ExceptionAddress));
	excpDescription.append(")");

	HandleExceptionInternal(false, ExceptionType::AUTO_UNHANDLED, new ExceptionInfo((char*)"", (char*)"", (char*)"", 0, (char*)ToString(GetCurrentThreadId()), (char*)excpDescription.c_str()));

	return EXCEPTION_EXECUTE_HANDLER;
}

void CARTWatchdog::HandleCRTException()
{
}

void CARTWatchdog::HandleMemAllocException(size_t _failedAllocSize)
{
}

void CARTWatchdog::HandleInvalidParamException(const wchar_t * expression, const wchar_t * function, const wchar_t * file, unsigned int line, uintptr_t pReserved)
{
}

void CARTWatchdog::HandleExceptionInternal(bool _isManual, ExceptionType _eType, ExceptionInfo* _eInfo)
{
	this->evtReportManager->GenerateEventReport(_isManual, _eType, _eInfo);
}

void CARTWatchdog::InitEventReportManager(CARTConfig * _cartCfg)
{
	this->evtReportManager = new EventReportManager(_cartCfg);

}

void CARTWatchdog::InstallExceptionHandlers()
{
	SetUnhandledExceptionFilter(&HandleSEHStatic);
}

LONG __stdcall HandleSEHStatic(PEXCEPTION_POINTERS pExceptionPtrs)
{
	if (cartWatchdog != nullptr) return cartWatchdog->HandleSEH(pExceptionPtrs);
	else return 0;
}