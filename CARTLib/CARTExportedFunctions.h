#pragma once

#include "CARTConfig.h"
#include "CARTLibExportDef.h"
#include "ExceptionInfo.h"
#include "ExceptionType.h"

typedef void(_cdecl *InitCARTLibInternal)(CARTConfig*);
typedef void(_cdecl *InvokeExceptionHandlerInternal)(ExceptionType _eType, ExceptionInfo* _eInfo);