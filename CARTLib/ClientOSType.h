#pragma once

#include "CARTLibExportDef.h"

enum CARTLIB_EXPORTS ClientOSType {
	UNKNOWN = 0,
	WIN = 1,
	MAC = 2,
	LINUX = 3
};