#include "EventReportManager.h"

#include <ctime>
#include <sstream>
#include <Softpub.h>

#include "resource.h"

#define ToString(obj) #obj

EventReportManager::EventReportManager(CARTConfig * _cartConfig)
{
	if (evtReportMgr != nullptr) return;

	this->cartCfg = _cartConfig;

	evtReportMgr = this;
	this->isUserInfoIncluded = false;
}

EventReportManager::~EventReportManager()
{
}

void EventReportManager::GenerateEventReport(bool _isManual, ExceptionType _eType, ExceptionInfo* _eInfo)
{
	string eventReportJsonStr = "";
	eventReportJsonStr.append("{ \"timestamp\": ");
	eventReportJsonStr.append(GetCurrentTimestamp());
	eventReportJsonStr.append(", \"event_type\": ");

	if (_isManual) eventReportJsonStr.append("\"exception\", ");
	else eventReportJsonStr.append("\"crash\", ");

	eventReportJsonStr.append("\"source_app_info\": { ");
	eventReportJsonStr.append("\"file\": \"");
	eventReportJsonStr.append(_eInfo->sourceFile);
	eventReportJsonStr.append("\", \"function\": \"");
	eventReportJsonStr.append(_eInfo->sourceFunction);
	eventReportJsonStr.append("()\", \"class\": \"");
	eventReportJsonStr.append(_eInfo->sourceClass);
	eventReportJsonStr.append("\", \"line\": ");
	eventReportJsonStr.append(ToString(_eInfo->sourceLine));
	eventReportJsonStr.append("\", \"app_codename\": \"");
	eventReportJsonStr.append(this->cartCfg->appInfo->appCodename);
	eventReportJsonStr.append("\", \"root_dir\": \"");

	char* currentDirStr = new char[MAX_PATH + 1];
	GetCurrentDirectoryA(MAX_PATH + 1, currentDirStr);

	eventReportJsonStr.append(currentDirStr);

	eventReportJsonStr.append("\", \"is_signed\": ");

	GUID verifyType = WINTRUST_ACTION_GENERIC_VERIFY_V2;
	WINTRUST_DATA verifyInfo;
	ZeroMemory(&verifyInfo, sizeof(WINTRUST_DATA));
	if (WinVerifyTrust(0, &verifyType, &verifyInfo) == 0) eventReportJsonStr.append("\"true\", ");
	else eventReportJsonStr.append("\"false\", ");
	eventReportJsonStr.append("\"image_type\": ");

	switch (this->cartCfg->appInfo->imageType) {
		case ClientAppImageType::EXE_BASE: {
			eventReportJsonStr.append("\"exe_base\", ");
			break;
		}
		case ClientAppImageType::DLL_BASE: {
			eventReportJsonStr.append("\"dll_base\", ");
			break;
		}
		case ClientAppImageType::EXE_DYNAMIC: {
			eventReportJsonStr.append("\"exe_dynamic\", ");
			break;
		}
		case ClientAppImageType::DLL_DYNAMIC: {
			eventReportJsonStr.append("\"dll_dynamic\", ");
			break;
		}
		default: {
			eventReportJsonStr.append("\"unknown\", ");
			break;
		}
	}

	eventReportJsonStr.append("\"image_name\": \"");
	eventReportJsonStr.append(this->cartCfg->appInfo->imageName);
	eventReportJsonStr.append("\", \"source_thread\": ");
	eventReportJsonStr.append(_eInfo->sourceThread);
	eventReportJsonStr.append("\", \"logs_available\": ");

	// TODO: Replace with working code, only for testing
	eventReportJsonStr.append("\"false\", ");

	eventReportJsonStr.append("\", \"callstack\": ");
	eventReportJsonStr.append("[ \"");

	// TODO: Replace with working code, only for testing
	eventReportJsonStr.append(_eInfo->sourceFunction);

	eventReportJsonStr.append("\", ], ");

	// TODO: Replace with working code, only for testing
	eventReportJsonStr.append("\"libs_loaded\": [], ");

	eventReportJsonStr.append("}, \"cart_description\": \"");
	eventReportJsonStr.append(_eInfo->description);
	eventReportJsonStr.append("\", \"cart_description_details\": \"");
	eventReportJsonStr.append(_eInfo->description);

	string dialogEventDescStr = "Application ";
	dialogEventDescStr.append(this->cartCfg->appInfo->appFullname);
	dialogEventDescStr.append(" has experienced a fatal crash.\nWe will automatically collect information about the app to assist in the resolution of this problem.\nYou can help us by providing additional details about the event.");
	this->dialogEventDescription = (char*)dialogEventDescStr.c_str();

	this->dHandle = CreateDialog(GetModuleHandle(0), MAKEINTRESOURCE(IDD_CART_WINDOW_CRASHINFO), 0, (DLGPROC)&DialogProcStatic);
	ShowWindow(this->dHandle, 0);

	eventReportJsonStr.append("\", \"user_info_included\": \"");
	if (this->isUserInfoIncluded) eventReportJsonStr.append("\"true\", ");
	else eventReportJsonStr.append("\"false\" }");

	this->eventReportJson = (char*)eventReportJsonStr.c_str();
}

void EventReportManager::SendReportToServer()
{
}

void EventReportManager::AddLogFile(char * _logFilePath)
{
}

void EventReportManager::GenerateReportJSON()
{
}

char * EventReportManager::ReadLogFile(char * _logFilePath)
{
	return nullptr;
}

void EventReportManager::GenerateAppScreenshot()
{
}

void EventReportManager::GetAppMemoryDump()
{
}

void EventReportManager::GetSystemInfo()
{
}

string EventReportManager::GetCurrentTimestamp()
{
	stringstream ss;
	ss << time(0);
	return ss.str();
}

BOOL EventReportManager::DialogProc(HWND dHandle, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg) {
		case WM_INITDIALOG: {
			SetDlgItemTextA(dHandle, IDC_CART_WINDOW_EVENT_DESCRIPTION, this->dialogEventDescription);

			return 1;
			break;
		}
		case WM_COMMAND: 
		{
			switch (LOWORD(wParam))
			{
				case IDC_CART_WINDOW_ADD_USER_INFO_CHECK:
				{
					switch (HIWORD(wParam))
					{
						case BN_CLICKED: {
							if (SendDlgItemMessage(dHandle, IDC_CART_WINDOW_ADD_USER_INFO_CHECK, BM_GETCHECK, 0, 0)) 
							MessageBox(NULL, L"Checkbox Selected", L"Success", MB_OK | MB_ICONINFORMATION);
							else
							MessageBox(NULL, L"Checkbox Unselected", L"Success", MB_OK | MB_ICONINFORMATION);
							break;
						}
					}
					break;
				} 
			}
		}
		default: {
			return DefDlgProcW(dHandle, uMsg, wParam, lParam);
		}
	}

	return 0;
}

BOOL CALLBACK DialogProcStatic(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (evtReportMgr != nullptr) return evtReportMgr->DialogProc(hwndDlg, uMsg, wParam, lParam);
	else return 0;
}
