#pragma once

#include "CARTLibExportDef.h"
#include "ClientOSType.h"
#include "ClientOSArch.h"

struct CARTLIB_EXPORTS ClientOSInfo {
public:
	ClientOSType osType;
	ClientOSArch osArch;
	char* osVersion;
};