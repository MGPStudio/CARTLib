#pragma once

#include <string>
#include <Windows.h>

#include "CARTConfig.h"
#include "ExceptionType.h"
#include "ExceptionInfo.h"

using namespace std;

class EventReportManager {
public:
	explicit EventReportManager(CARTConfig* _cartConfig);
	~EventReportManager();

	void GenerateEventReport(bool _isManual, ExceptionType _eType, ExceptionInfo* _eInfo);
	void SendReportToServer();

	void AddLogFile(char* _logFilePath);

	BOOL CALLBACK DialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
private:
	void GenerateReportJSON();
	char* ReadLogFile(char* _logFilePath);
	void GenerateAppScreenshot();
	void GetAppMemoryDump();
	void GetSystemInfo();

	string GetCurrentTimestamp();	

	CARTConfig* cartCfg;
	char** logFiles;
	unsigned int logFileCount;
	char* eventReportJson;
	char* dialogEventDescription;
	bool isUserInfoIncluded;
	HWND dHandle;
};

static EventReportManager* evtReportMgr = nullptr;

static BOOL CALLBACK DialogProcStatic(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);