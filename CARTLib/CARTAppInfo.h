#pragma once

#include "CARTLibExportDef.h"
#include "ClientAppImageType.h"

struct CARTAppInfo {
public:
	CARTAppInfo(char* _appCodename, char* _appFullname, unsigned int _appVersionMajor, unsigned int _appVersionMinor, unsigned int _appVersionBuildId, ClientAppImageType _imageType, char* _imageName) {
		this->appCodename = _appCodename;
		this->appFullname = _appFullname;
		this->appVersionMajor = _appVersionMajor;
		this->appVersionMinor = _appVersionMinor;
		this->appVersionBuildId = _appVersionBuildId;
		this->imageType = _imageType;
		this->imageName = _imageName;
	}

	char* appCodename;
	char* appFullname;
	unsigned int appVersionMajor;
	unsigned int appVersionMinor;
	unsigned int appVersionBuildId;
	ClientAppImageType imageType;
	char* imageName;
};