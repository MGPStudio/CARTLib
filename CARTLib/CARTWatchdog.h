#pragma once

#include <Windows.h>

#include "ExceptionType.h"
#include "ExceptionInfo.h"
#include "CARTConfig.h"
#include "EventReportManager.h"

class CARTWatchdog {
public:
	explicit CARTWatchdog(CARTConfig* _cartConfig);
	~CARTWatchdog();

	void AddLogFile(char* _logFilePath);
	void HandleManualException(ExceptionType _eType, ExceptionInfo* _eInfo);
	
	LONG WINAPI HandleSEH(PEXCEPTION_POINTERS pExceptionPtrs);
private:
	void HandleExceptionInternal(bool _isManual, ExceptionType _eType, ExceptionInfo* _eInfo);

	void HandleCRTException();
	void HandleMemAllocException(size_t _failedAllocSize);
	void HandleInvalidParamException(const wchar_t* expression, const wchar_t* function, const wchar_t* file, unsigned int line, uintptr_t pReserved);

	void InitEventReportManager(CARTConfig* _cartCfg);
	void InstallExceptionHandlers();

	EventReportManager* evtReportManager = nullptr;
	CARTConfig* cartConfig = nullptr;
};

static CARTWatchdog* cartWatchdog = nullptr;

static LONG __stdcall HandleSEHStatic(PEXCEPTION_POINTERS pExceptionPtrs);