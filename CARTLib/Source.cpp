#include "CARTLibExportDef.h"
#include "CARTConfig.h"

#include "CARTWatchdog.h"

#include <Windows.h>

CARTConfig cartCfg;

CARTWatchdog* cartWdg;

void InitCARTLibInternal(CARTConfig* _cartCfg) {
	if (_cartCfg != nullptr) cartWdg = new CARTWatchdog(_cartCfg);
	else {
		MessageBox(nullptr, L"Client App Reporting Tool Initialization Error.\nReason: Invalid CARTConfig structure provided", L"CART Initialization Error", MB_OK | MB_ICONERROR);
		ExitProcess(-1);
	}
}

void InvokeExceptionHandlerInternal(ExceptionType _eType, ExceptionInfo* _eInfo) {
	cartWdg->HandleManualException(_eType, _eInfo);
}