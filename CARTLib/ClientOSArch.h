#pragma once

#include "CARTLibExportDef.h"

enum CARTLIB_EXPORTS ClientOSArch {
	UNKNOWN = 0,
	X86 = 1,
	X64 = 2
};