#pragma once

#include "CARTLibExportDef.h"

enum CARTLIB_EXPORTS ClientAppImageType {
	UNKNOWN = 0,
	EXE_BASE = 1,
	DLL_BASE = 2,
	EXE_DYNAMIC = 3,
	DLL_DYNAMIC = 4
};