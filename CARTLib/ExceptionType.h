#pragma once

#include "CARTLibExportDef.h"

enum CARTLIB_EXPORTS ExceptionType {
	MANUAL_DIV_BY_ZERO = 0,
	AUTO_INVALID_PARAMETER = 1,
	AUTO_CRT_ERROR = 2,
	AUTO_MEM_ALLOC_ERROR = 3,
	AUTO_UNHANDLED = 4
};