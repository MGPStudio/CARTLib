#pragma once

#include <Windows.h>
#include <string>

#include "../CARTLib/CARTExportedFunctions.h"

HINSTANCE cartLibHandle;

using namespace std;

void InitCARTLib(CARTConfig* cartCfg) {
	cartLibHandle = LoadLibrary(L"CARTLib.dll");
	if (!cartLibHandle) {
		MessageBox(nullptr, L"Client App Reporting Tool Initialization Error.\nReason: Failed to load the CART library.", L"CART Initialization Error", MB_OK | MB_ICONERROR);
		FreeLibrary(cartLibHandle);
		ExitProcess(-1);
	}
	InitCARTLibInternal InitFunction = (InitCARTLibInternal)GetProcAddress(cartLibHandle, "InitCARTLibInternal");
	cartCfg->appInfo = new CARTAppInfo("undefined", "undefined", 0, 1, 1, ClientAppImageType::UNKNOWN, "undefined");

	string imageName = new char[MAX_PATH + 1];

	GetModuleFileNameA(0, (char*)imageName.c_str(), MAX_PATH + 1);

	imageName.erase(0, imageName.find_last_of("\\") + 1);

	cartCfg->appInfo->imageName = (char*)imageName.c_str();

#ifdef CART_APP_IMAGE_TYPE
	cartCfg->appInfo->imageType = CART_APP_IMAGE_TYPE;
#endif

#ifdef CART_APP_CODE_NAME
	cartCfg->appInfo->appCodename = CART_APP_CODE_NAME;
#endif

#ifdef CART_APP_FULL_NAME
	cartCfg->appInfo->appFullname = CART_APP_FULL_NAME;
#endif
	
	InitFunction(cartCfg);
}

#define ToString(obj) #obj

#define InvokeCARTExceptionHandler(type, description) InvokeExceptionHandler(type, new ExceptionInfo((char*)__FILE__, (char*)__FUNCTION__, (char*)"undefined", __LINE__, "main", (char*)description))

void InvokeExceptionHandler(ExceptionType _eType, ExceptionInfo* _eInfo) {
	InvokeExceptionHandlerInternal InvokeLibHandler = (InvokeExceptionHandlerInternal)GetProcAddress(cartLibHandle, "InvokeExceptionHandlerInternal");
	InvokeLibHandler(_eType, _eInfo);
}