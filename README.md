**CARTLib - Client App Reporting Tool Library**  
  
Includes:  
  
- Source Code (C++)  
  
This repository contains the source code of CARTLib for V.N. Karazin Kharkiv National University. Only for educational use.  
  
(c) MGP Studio, 2019. All Rights Reserved.